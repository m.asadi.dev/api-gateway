let mtc = require('../service/decorator').mtc
module.exports={
  add(req, res, next) {
    if (!req.body.followerId) { return res.json(mtc(false, 'followerId_is_required')) }
    next()
  },
  remove(req, res, next) {
    if (!req.body.followerId) { return res.json(mtc(false, 'followerId_is_required')) }
    next()
  },
}