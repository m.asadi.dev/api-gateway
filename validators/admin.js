let mtc = require('../service/decorator').mtc
module.exports = {
  register(req, res, next) {
    if (!req.body.username) { return res.json(mtc(false, 'username_is_required')) }
    if(!req.body.password){return res.json(mtc(false, 'password_is_required'))}
    next()
  },
  login(req, res, next) {
    if (!req.body.username) { return res.json(mtc(false, 'username_is_required')) }
    if(!req.body.password){return res.json(mtc(false, 'password_is_required'))}
    next()
  },
  changePassword(req, res, next) {
    if (!req.body.currentPassword) { return res.json(mtc(false, 'currentPassword_is_required')) }
    if(!req.body.newPassword) {return res.json(mtc(false,'newPassword_is_required'))}
    next()
  },
}