let mtc = require('../service/decorator').mtc
module.exports = {
  register(req, res, next) {
    if (!req.body.email) { return res.json(mtc(false, 'email_is_required')) }
    if (!validateEmail(req.body.email)) { return res.json(mtc(false, 'email_validation_failed')) }
    next()
  },
  verify(req, res, next) {
    if (!req.body.email) { return res.json(mtc(false, 'email_is_required')) }
    if (!validateEmail(req.body.email)) { return res.json(mtc(false, 'email_validation_failed')) }
    if (!req.body.verificationCode) { return res.json(mtc(false, 'verificationCode_is_required')) }
    next()
  },
  createPassword(req, res, next) {
    if (!req.body.password) { return res.json(mtc(false, 'password_is_required')) }
    if (req.body.password.length < 4) { return res.json(mtc(false, 'password_min_length_4')) }
    next()
  },
  login(req, res, next) {
    if (!req.body.email) { return res.json(mtc(false, 'email_is_required')) }
    if (!validateEmail(req.body.email)) { return res.json(mtc(false, 'email_validation_failed')) }
    if (!req.body.password) { return res.json(mtc(false, 'password_is_required')) }
    next()
  },
  forgetPassword(req, res, next) {
    if (!req.body.email) { return res.json(mtc(false, 'email_is_required')) }
    if (!validateEmail(req.body.email)) { return res.json(mtc(false, 'email_validation_failed')) }
    next()
  },
  changePassword(req, res, next) {
    if (!req.body.currentPassword) { return res.json(mtc(false, 'currentPassword_is_required')) }
    if(!req.body.newPassword) {return res.json(mtc(false,'newPassword_is_required'))}
    next()
  },
  search(req,res,next){
    let text=req.query.text
    if(!text){return res.json(mtc(false, 'text_is_required')) }
    if(text.length<3){return res.json(mtc(false, 'text_min_length_3')) }

  }
}
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}