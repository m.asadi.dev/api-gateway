const mtc = require('../service/decorator').mtc
const bcrypt = require('bcrypt')
const Admin = require('../model/Admin')
const jwt = require('../service/jwt-auth')
const saltRounds = 10
module.exports = {
  async register(req, res) {
    const username = req.body.username
    let fAdmin = await Admin.findOne({ username })
    if (fAdmin) { return res.json(mtc(false, 'username_existed')) }
    
    req.body.password = await bcrypt.hash(req.body.password, saltRounds)
    try {
      let admin = new Admin(req.body)
      await admin.save()
      res.json(mtc(true, 'admin_is_added'))
    } catch (error) {
      console.log(error)
      res.json(mtc(false, error.message))
    }
  },
  async login(req, res) {
    const { username, password } = req.body
    let fAdmin = await Admin.findOne({ username })
    if(!fAdmin){return res.status(404).json(mtc(false, 'admin_not_found'))}
    if (!await bcrypt.compare(password, fAdmin.password)) {
      return res.status(401).json(mtc(false, 'failed_login'))
    }
    let packet = jwt.sign({ id: fAdmin._id, is: 'admin',})
    res.json(mtc(true,'logined',packet))
  },
  async changePassword(req,res){
    const decoded=req.user;
    const currentPassword=req.body.currentPassword;
    let newPassword=req.body.newPassword;
    let admin = await Admin.findById(decoded.id)
    if(!admin){return res.status(404).json(mtc(false, 'admin_not_found'))}
    if (!await bcrypt.compare(currentPassword, admin.password)) {
      return res.status(401).json(mtc(false, 'currentPassword_incorrect'))
    }
    newPassword = await bcrypt.hash(newPassword, saltRounds)
    admin.password=newPassword;
    await admin.save()
    res.json(mtc(true, 'password_changed'));
    
  }
}