let Follower=require('../model/Follower')
let User=require('../model/User')
const mtc = require('../service/decorator').mtc
module.exports={
 async FollowingList(req,res){
    let decoded=req.user
    const options = {
      page: 1,
      limit: 30,
      populate: 'userId',
      select: '-password -userId.password'
    };
    if (req.query.page && req.query.page > 1) {
      options.page = req.query.page
    }
    try {
      let following = await Follower.paginate({followerId:decoded.id}, options)
      following.docs.forEach(element => {
         element.userId.password=""
      });
      res.json(mtc(true, 'following', following))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
 async FollowerList(req,res){
    let decoded=req.user
    const options = {
      page: 1,
      populate: 'followerId',
      limit: 30,
      lean:true,
    };
    if (req.query.page && req.query.page > 1) {
      options.page = req.query.page
    }
    try {
      let following = await Follower.paginate({userId:decoded.id}, options)
      following.docs.forEach(element => {
        if(element.followerId&&element.followerId.password){
          element.followerId.password=""

        }
      });
      
      res.json(mtc(true, 'followers', following))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
 async add(req,res){
    let decoded=req.user
    const followerId=req.body.followerId
    try {
      let user=await User.findById(followerId)
      if(decoded.id===followerId){
        return res.status(403).json(mtc(false, 'not_follow_self'))
      }
      if(user.role!=2){
        return res.status(403).json(mtc(false, 'user_not_permission'))
      }
      let follower=new Follower({userId:decoded.id,followerId})
      await follower.save()
      return res.status(200).json(mtc(true, 'user_is_follow'))

    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
  async remove(req,res){
    const followerId=req.body.followerId
    const userId=req.user.id
    try {
      await Follower.deleteOne({userId,followerId})
      return res.status(200).json(mtc(true, 'user_is_unfollow'))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
  async followingListById(req,res){
    let following= await Follower.find({userId:req.params.id})
    res.json(mtc(true,'following',following))
  }
}
