const User = require('../model/User')
const mtc = require('../service/decorator').mtc
const redisClient = require('../service/redis')
const sendVerifyCodeByEmail = require('../service/email').sendVerifyCode
const generateNumber = require('../service/generate').generateNumber
const jwt = require('../service/jwt-auth')
const bcrypt = require('bcrypt')
const aws = require('aws-sdk')
const saltRounds = 10
aws.config.setPromisesDependency()
aws.config.update({
  accessKeyId: global.ACCESSKEYID,
  secretAccessKey: global.SECRETACCESSKEY,
  region:global.REGION,
  s3ForcePathStyle: true,
  signatureVersion: 'v4'
})
function getType(text = "") {
  return text.substring(text.lastIndexOf('.'))
}
module.exports = {
  async list(req, res) {
    const options = {
      page: 1,
      limit: 30,
      select: '-password'
    };
    if (req.query.page && req.query.page > 1) {
      options.page = req.query.page
    }
    try {
      let users = await User.paginate({}, options)
      res.json(mtc(true, 'users', users))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
  async search(req, res) {
    const options = {
      page: 1,
      limit: 30,
      select: '-password'
    };
    const query={
      role:2,
      username: { "$regex": req.query.text, "$options": "i" },
    }
    
    if (req.query.page && req.query.page > 1) {
      options.page = req.query.page
    }
    try {
      let users = await User.paginate(query, options)
      res.json(mtc(true, 'users', users))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }
  },
  async edit(req, res) {
    let decoded = req.user
    let _id = req.params.id
    let { firstname, lastname, isBanned } = req.body
    let packet = { firstname, lastname }
    if (decoded.is == jwt.roles.user) {
      _id = decoded.id
    } else {
      packet.isBanned = isBanned
    }
    if (!_id) { return res.json(mtc(false, '_id_required')) }

    await User.updateOne({ _id }, packet)
    res.json(mtc(true, 'user_edited'))

  },
  async details(req, res) {
    let decoded = req.user
    let _id = req.params.id
    if (decoded.is == jwt.roles.user) {
      _id = decoded.id
    }
    if (!_id) { return res.json(mtc(false, '_id_required')) }
    try {
      let user = await User.findById(_id).select('-password')
      res.json(mtc(true, 'user', user))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }


  },
  async register(req, res) {
    try {
      const email = req.body.email;
      let user = await User.findOne({ email })
      if (user) { return res.json(mtc(false, 'user_exist')) }
      const verifyCode = generateNumber(11111, 99999)
      await redisClient.setItem(email, verifyCode)
      await sendVerifyCodeByEmail(email, verifyCode)
      res.json(mtc(true, 'sended_verificationCode'));
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }

  },
  async verify(req, res) {
    try {
      const email = req.body.email
      const verificationCode = req.body.verificationCode
      let validVerificationCode = await redisClient.getItem(email)
      if (!validVerificationCode) { return res.json(mtc(false, 'user_not_found')) }
      if (validVerificationCode != verificationCode) {
        return res.status(401).json(mtc(false, 'failed_login'))
      }
      let user = await User.findOne({ email })
      if (user) {
        user.password = ''
        await user.save()
      } else {
        user = new User({ email, provider: 'email', role: 1 })
        await user.save();
      }
      let packet = jwt.sign({ id: user._id, is: 'user', role: user.role, provider: user.provider })
      await redisClient.deleteItem(email)
      return res.json(mtc(true, 'logined', packet))
    } catch (error) {
      console.log(error)
      res.status(500).json(mtc(false, error.message))
    }

  },
  async createPassword(req, res) {
    let decoded = req.user;
    try {
      let user = await User.findById(decoded.id)
      if (!user) { res.status(404).json(mtc(false, 'user_not_found')) }
      if (user.password && user.password != '') { res.status(403).json(mtc(false, 'password_is_exist')) }
      user.password = await bcrypt.hash(req.body.password, saltRounds)
      await user.save()
      res.json(mtc(true, 'password_created'))
    } catch (error) {
      console.log(error)
      return res.status(500).json(mtc(false, error.message))
    }
  },
  async login(req, res) {
    let { email, password } = req.body;
    try {
      let user = await User.findOne({ email })
      if (!await bcrypt.compare(password, user.password)) {
        return res.status(401).json(mtc(false, 'failed_login'))
      }
      let packet = jwt.sign({ id: user._id, is: 'user', role: user.role, provider: user.provider })
      return res.json(mtc(true, 'logined', packet))
    } catch (error) {
      console.log(error)
      return res.status(500).json(mtc(false, error.message))
    }
  },
  async forgetPassword(req, res) {
    let { email } = req.body;
    try {
      let user = await User.findOne({ email })
      if (!user) {
        return res.json(mtc(true, 'account_not_found'))
      }
      const verifyCode = generateNumber(11111, 99999)
      await redisClient.setItem(email, verifyCode)
      await sendVerifyCodeByEmail(email, verifyCode)
      res.json(mtc(true, 'sended_verificationCode'));
    } catch (error) {
      console.log(error)
      return res.status(500).json(mtc(false, error.message))
    }
  },
  async changePassword(req, res) {
    const decoded = req.user;
    const currentPassword = req.body.currentPassword;
    let newPassword = req.body.newPassword;
    let user = await User.findById(decoded.id)
    if (!await bcrypt.compare(currentPassword, user.password)) {
      return res.status(401).json(mtc(false, 'currentPassword_incorrect'))
    }
    newPassword = await bcrypt.hash(newPassword, saltRounds)
    user.password = newPassword;
    await user.save()
    res.json(mtc(true, 'password_changed'));
  },
  async avatar(req, res) {
    const decoded = req.user
    const user = await User.findById(decoded.id)
    if (!user) {
      return res.status(404).json(mtc(false, 'user_not_found'))
    }
    if (!req.file) {
      return res.status(404).json(mtc(false, 'avatar_is_required'))
    }
    const s3 = new aws.S3()

    const file = req.file
    const params = {
      Bucket: global.BUCKET_NAME,
      Key: "avatar-" + user._id + getType(file.originalname),
      Body: file.buffer,
      ACL: 'public-read'
    };
    s3.upload(params, async function (err, data) {
      if (err) {
        res.json({ "error": true, "Message": err });
      } else {
        user.avatar = {
          url: data.Location, key: data.key
        }
        await user.save()
        res.json(mtc(true, "avatar", { url: data.Location, key: data.key }))
      }
    });
  }

}