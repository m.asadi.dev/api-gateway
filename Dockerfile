# Install node v14
FROM node:14

# Set the workdir /var/www/api-gateway
WORKDIR /var/www/api-gateway

# Copy the package.json to workdir
COPY package.json ./

# Run npm install - install the npm dependencies
RUN npm cache clean --force && rm -rf node_modules && npm install


# Copy application source
COPY . .


# Expose application ports - (8080 - for API )
EXPOSE 8080


# Start the application
CMD ["npm", "start"]