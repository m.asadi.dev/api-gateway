const mongoose=require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { Schema } = mongoose
const followerSchema = new Schema({
  userId:{type:Schema.Types.ObjectId,ref:'User'},
  followerId:{type:Schema.Types.ObjectId,ref:'User'}
});
followerSchema.index({ userId: 1, followerId: 1 }, { unique: true });

followerSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Follower', followerSchema);