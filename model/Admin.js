const mongoose=require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { Schema } = mongoose
const adminSchema = new Schema({
  username:{type:Schema.Types.String,maxlength:30},
  password:{type:Schema.Types.String},
});

adminSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Admin', adminSchema);