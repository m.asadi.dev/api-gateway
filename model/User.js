const mongoose=require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { Schema } = mongoose
const userSchema = new Schema({
  firstname:{type:Schema.Types.String,maxlength:30},
  lastname:{type:Schema.Types.String,maxlength:40},
  avatar:{
    url:{type:String},
    key:{type:String}
  },
  bio:{type:Schema.Types.String,maxlength:150},
  role:{type:Schema.Types.Number,enum:[0,1,2]},
  username:{type:Schema.Types.String,maxlength:30},
  isBanned:{type:Schema.Types.Boolean,default:false},
  email:{type:Schema.Types.String,maxlength:100},
  phoneNumber:{type:Schema.Types.String,maxlength:20},
  password:{type:Schema.Types.String},
  provider:{type:Schema.Types.String,enum:['email','sms','google','facebook']}
});

userSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', userSchema);