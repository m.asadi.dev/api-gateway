
var nodemailer = require('nodemailer');

var ejs = require("ejs");

module.exports.sendVerifyCode =async function (to,code) {
    const subject="verification code"
    let html= await ejs.renderFile(process.cwd()+"/views/emailveryfication.ejs", { code: code });

    send(to,subject,html)
}


  function send(to, subject, html) {
    var transporter = nodemailer.createTransport({
        service:"gmail",
        auth: {
            user: global.EMAIL,
            pass: global.EMAIL_PASS
        }
    });

    var mailOptions = {
        from: global.EMAIL,
        to: to,
        subject: subject,
        html: html
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

}