const mongoose = require('mongoose')
module.exports.connect =async () => {
  try {
    await mongoose.connect(global.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('Now connected to MongoDB!')
  } catch (error) {
    console.log(error)
  }

}