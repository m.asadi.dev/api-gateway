const asyncRedis = require("async-redis");
const client = asyncRedis.createClient(global.REDIS_URL);
client.on("error", function (err) {
  console.log("Error " + err);
});

module.exports.setItem=async(key,value)=>{
  await client.set(key, value);

}
module.exports.getItem=async(key)=>{
  return await client.get(key);
}
module.exports.deleteItem=async(key)=>{
  await client.del(key)

}