const jwt = require('jsonwebtoken')
const fs = require('fs')
const JWTpublicKey = fs.readFileSync(global.PUBLIC_KEY);
const JWTprivateKey = fs.readFileSync(global.PRIVATE_KEY);
const mtc=require('../service/decorator').mtc
const User=require('../model/User')
module.exports = {
  roles:{
    admin:'admin',
    user:'user'
  },
  userRoles:{
    normal:1,
    artist:2
  },
  sign(user) {
    const token = jwt.sign({...user,type:'token'}, JWTprivateKey, { expiresIn: '30d', algorithm: 'RS256' });
    const refreshToken = jwt.sign({...user,type:'refresh'}, JWTprivateKey, { expiresIn: '60d', algorithm: 'RS256' });
    const packet = {
        "token": "bearer " + token,
        "refreshToken": "bearer " + refreshToken,
    }
    return packet
  },
  verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader != 'undefined') {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, JWTpublicKey, (err, authData) => {
        if (err) {
          if (err.name === "TokenExpiredError") {
            res.statusCode = 401;
            res.send(mtc(false, err.name, {}));
          } else {
            res.statusCode = 401;
            res.send(mtc(false, err.message, {}));
          }

        } else {
          req.user = authData
          next();
        }
      });


    } else {
      res.sendStatus(401);
      res.send(mtc(false, 'token_expire', {}));


    }
  },
   checkRole (roles,usrRoles=[1]) {
    return async function (req, res, next) {
      const is=req.user.is
      if(!roles.find(element => element === is)){
        return res.json(mtc(false,'access_denid'))
      }
      if(is!==roles.admin){
        let user=await User.findById(req.user.id)
        if(!user){
          return res.json(mtc(false,'user_not_found'))

        }
        if(!usrRoles.find(element => element === user.role)){
          return res.json(mtc(false,'access_denid'))
        }
      }
      next()
    }
  }
}