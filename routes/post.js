let express = require('express')
let router = express.Router()
const { createProxyMiddleware } = require('http-proxy-middleware');
const jwt = require('../service/jwt-auth')
const microservices = require('../microservices')
router.post('/', jwt.verifyToken, jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist]), createProxyMiddleware({ target: microservices.services.post.url, changeOrigin: true, }))
router.get('/:id', createProxyMiddleware({ target: microservices.services.post.url }))
router.delete('/:id', jwt.verifyToken, jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist]), createProxyMiddleware({target: microservices.services.post.url}))
router.get('/following', jwt.verifyToken, jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist]), createProxyMiddleware({ target: microservices.services.post.url, changeOrigin: true, }))
router.post('/:id/likes', jwt.verifyToken, jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist,jwt.userRoles.normal]), createProxyMiddleware({ target: microservices.services.post.url, changeOrigin: true, }))
router.get('/:id/likes/count',createProxyMiddleware({ target: microservices.services.post.url, changeOrigin: true, }))
router.get('/:id/likes',createProxyMiddleware({ target: microservices.services.post.url, changeOrigin: true, }))

module.exports = router