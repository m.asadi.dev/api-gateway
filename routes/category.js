let express = require('express')
let router = express.Router()
const { createProxyMiddleware } = require('http-proxy-middleware');
const jwt = require('../service/jwt-auth')
const microservices = require('../microservices')
router.post('/', jwt.verifyToken, jwt.checkRole([jwt.roles.admin]), createProxyMiddleware({ target: microservices.services.category.url }))
router.put('/:id', jwt.verifyToken, jwt.checkRole([jwt.roles.admin]),createProxyMiddleware({ target: microservices.services.category.url }))
router.get('/',createProxyMiddleware({ target: microservices.services.category.url }))
router.delete('/:id', jwt.verifyToken, jwt.checkRole([jwt.roles.admin]), createProxyMiddleware({target: microservices.services.category.url}))
module.exports = router