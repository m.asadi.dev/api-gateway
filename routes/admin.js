const express = require('express');
let router = express.Router();
let controller=require('../controllers/admin')
let validation=require('../validators/admin')
let jwt=require('../service/jwt-auth')

router.post('/auth/register',jwt.verifyToken,jwt.checkRole([jwt.roles.admin]),validation.register,controller.register)
router.post('/auth/login',validation.login,controller.login)
router.post('/auth/change-password',jwt.verifyToken,jwt.checkRole([jwt.roles.admin]),validation.changePassword,controller.changePassword)

module.exports = router