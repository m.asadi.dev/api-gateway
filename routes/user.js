const express = require('express');
let router = express.Router();
let controller=require('../controllers/user')
let followController=require('../controllers/follow')

let validation=require('../validators/user')
let followValidation=require('../validators/follow')

let jwt=require('../service/jwt-auth')
const multer = require('multer')

var storage = multer.memoryStorage({
  destination: function(req, file, callback) {
      callback(null, '');
  }
})
router.get('/',jwt.verifyToken,jwt.checkRole([jwt.roles.admin]),controller.list)
router.get('/search',controller.search,validation.search)
router.post('/followers',jwt.verifyToken,jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist]),followValidation.add,followController.add)
router.get('/followers',jwt.verifyToken,jwt.checkRole([jwt.roles.user],[jwt.userRoles.normal,jwt.userRoles.artist]),followController.FollowerList)
router.get('/following',jwt.verifyToken,jwt.checkRole([jwt.roles.user],[jwt.userRoles.normal,jwt.userRoles.artist]),followController.FollowingList)
router.get('/following/:id',followController.followingListById)

router.post('/unfollow',jwt.verifyToken,jwt.checkRole([jwt.roles.user],[jwt.userRoles.normal,jwt.userRoles.artist]),followValidation.remove,followController.remove)

router.get('/:id',jwt.verifyToken,jwt.checkRole([jwt.roles.admin,jwt.roles.user],[jwt.userRoles.normal,jwt.userRoles.artist]),controller.details)
router.put('/:id',jwt.verifyToken,jwt.checkRole([jwt.roles.admin,jwt.roles.user],[jwt.userRoles.normal,jwt.userRoles.artist]),controller.edit)
router.post('/avatar',jwt.verifyToken,jwt.checkRole([jwt.roles.user],[jwt.userRoles.artist]),multer({ storage: storage }).single('avatar'),controller.avatar)
router.get('/',controller.list,jwt.verifyToken,jwt.checkRole([jwt.roles.admin]))

router.post('/auth/register',validation.register,controller.register)
router.post('/auth/verify',validation.verify,controller.verify)
router.post('/auth/login',validation.login,controller.login)
router.post('/auth/forget-password',validation.forgetPassword,controller.forgetPassword)
router.post('/auth/create-password',jwt.verifyToken,jwt.checkRole([jwt.roles.user]),validation.createPassword,controller.createPassword)
router.post('/auth/change-password',jwt.verifyToken,jwt.checkRole([jwt.roles.user]),validation.changePassword,controller.changePassword)


module.exports = router